using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Human : Creature
{
    [SerializeField] private int _agility;

    public override void TakeDamage(int damage)
    {
        damage /= _agility;
        Health -= damage;
        TryDie();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wombat : Creature
{
    [SerializeField] private int _armor;

    public override void TakeDamage(int damage)
    {
        damage -= _armor;
        Health -= damage;
        TryDie();
    }
}

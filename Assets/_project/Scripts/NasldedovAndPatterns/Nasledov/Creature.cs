using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creature : MonoBehaviour
{

    public int Health;

    public void TryDie()
    {
        if (Health <= 0)
        {
            Debug.Log("� ����");
        }
    }

    public virtual void TakeDamage(int damage)
    {
        Health -= damage;
        TryDie();
    }
}

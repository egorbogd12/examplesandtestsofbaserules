using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Player))]
public class SlowMotionDecorator : Player
{
    
    private float _timeScale = 2f;

    public override void Move()
    {
        if (Input.GetKey(KeyCode.Space) )
        {
            Time.timeScale = _timeScale;
            Debug.Log("����� ����������");
        }
        base.Move();
    }
}

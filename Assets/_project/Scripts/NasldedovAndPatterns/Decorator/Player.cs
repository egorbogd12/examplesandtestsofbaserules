using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private float _moveSpeed = 3f;


    private void Start()
    {
    }

    private void Update()
    {
        Move();
    }

    public virtual void Move()
    {
        transform.Translate(Input.GetAxis("Horizontal") * _moveSpeed * Time.deltaTime, 0, Input.GetAxis("Vertical") * _moveSpeed * Time.deltaTime);
    }

}

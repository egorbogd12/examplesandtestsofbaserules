using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creature1 : MonoBehaviour
{
    [SerializeField] private int _health;

    public void TakeDamage(int damage)
    {
        _health -= AffectDamage(damage);
        TryDie();
        if (_health < 0)
        {
            _health = 0;
        }
    }

    protected virtual int AffectDamage(int damage)
    {
        return damage;
    }

    private void TryDie()
    {
        if (_health <= 0)
        {
            Debug.Log("� ����");
        }
    }

    // ������� ������������ ������� ������� , �� ���������� ����� ������ ������������ ���� ����� �������.  
}

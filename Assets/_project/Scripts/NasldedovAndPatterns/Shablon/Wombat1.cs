using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wombat1 : Creature1
{
    [SerializeField] private int _armor;

    protected override int AffectDamage(int damage)
    {
       return damage -= _armor;
    }
}

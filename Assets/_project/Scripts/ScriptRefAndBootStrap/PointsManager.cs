using UnityEngine;

public class PointsManager : MonoBehaviour
{
    public static PointsManager instance;


    [SerializeField] private int _startPoints;
    
    public int Points { get; private set; }
 

    //private void Awake()
    //{
    //    instance = this;
    //    Points = _startPoints;
    //}
    
    public void AddPoints()
    {
        Points++;
        Debug.Log("+");
    }
    public void MinusPoints()
    {
        Points--;
        Debug.Log("-");
    }

    public void Initialize() // ������ �������, �� ������ ��� 
    {
        instance = this;
        Points = _startPoints;
    }
}

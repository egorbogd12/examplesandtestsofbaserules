using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitManager : MonoBehaviour
{
    [SerializeField] private Unit _unitPrefab;

    private Unit _spawnedObj; // ������1. ���� 2 ������ �� �������� , �������� ��� � ��� ������ � ����������� ����� ���� .
  
    void Start()
    {
        _spawnedObj = Instantiate(_unitPrefab);
        _spawnedObj.Level = 1; // ������1
    }


    void Update()
    {
        _spawnedObj.transform.position = Random.insideUnitSphere;
    }
}

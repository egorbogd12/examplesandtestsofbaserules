using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitManager1 : MonoBehaviour
{
    [SerializeField] private Unit1 _unitPrefab;  // ������ � ��������� ��������

    private Unit1 _spawnedObj;


    private List<Unit1> _units = new List<Unit1> ();


    void Start()
    {
        _spawnedObj = Instantiate(_unitPrefab);

        for (int i = 0; i < 10; i++)
        {
            _units.Add(Instantiate(_unitPrefab));
        }
    }

    // Update is called once per frame
    void Update()
    {
        _spawnedObj.transform.position = Random.insideUnitSphere;
        foreach (var unit1 in _units)
        {
            unit1.transform.position = Random.insideUnitSphere;

        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SmartHouse : MonoBehaviour
{
    [Range(0, 100)]
    [SerializeField] private int _temp = 20;
    public ISmartHouse[] smartHouseParts;

    // Start is called before the first frame update

    private void Awake()
    {
        smartHouseParts = FindObjectsOfType<MonoBehaviour>().OfType<ISmartHouse>().ToArray();
    }
    void Start()
    {
        // smartHouseParts = FindObjectsOfType<MonoBehaviour>().OfType<ISmartHouse>().ToArray();

    }
    public void OnStart()
    {

        foreach (var part in smartHouseParts)
        {
            part.TurnOn();
        }

    }

    public void StartFreeze()
    {
        foreach (var part in smartHouseParts)
        {
            part.Freeze();
            part.Freeze(_temp);
        }

    }
}

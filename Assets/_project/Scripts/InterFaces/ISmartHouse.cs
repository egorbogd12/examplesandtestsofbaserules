using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISmartHouse 
{
    public void TurnOn();

    public void Freeze();

    public void Freeze(int temparature);

    public void TurnOff();
}

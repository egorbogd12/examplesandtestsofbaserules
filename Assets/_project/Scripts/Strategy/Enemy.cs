using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : MonoBehaviour
{
    public float _health;
    public float _armor;

    protected IAttackStrategy _attackStrategy;

    public void SetAttackStrategy(IAttackStrategy strategy)
    {
        _attackStrategy = strategy;
    }

    public void Attacking()
    {
        _attackStrategy.Attack();
    }
}

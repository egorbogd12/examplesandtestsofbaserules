using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goblin : Enemy
{

    [SerializeField, Range(1, 2)] private int _typeOfAttack;


    private void Update()
    {
        switch (_typeOfAttack)
        {
            case 1: SetAttackStrategy(new MeleeAttackStrategy());
                break;

            case 2: SetAttackStrategy(new RangeAttackStrategy());
                break;
            default:
                break;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Attacking();
        }
    }
}
